 
 package models
 
 type CreateProfessionModel struct {
 	Name string `json:"name" binding:"required"`
 }
 
 type Profession struct {
 	Id   string `json:"id"`
 	Name string `json:"name"`
 }
 
 type GetAllProfessionResponse struct {
 	Professions []Profession `json:"professions"`
 	Count       uint32       `json:"count"`
 }
package models

type CreateAttributeModel struct {
	Name          string `json:"name" binding:"required"`
	AttributeType string `json:"attribute_type" binding:"required"`
}

type Attribute struct {
	ID            string `json:"id"`
	Name          string `json:"name"`
	AttributeType string `json:"attribute_type"`
}

type GetAllAttributeResponse struct {
	Attribute []Attribute `json:"attributes"`
	Count     uint32      `json:"count"`
}

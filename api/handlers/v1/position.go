package v1

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/udevs/example_api_gateway/api/models"
	"bitbucket.org/udevs/example_api_gateway/genproto/position_service"
	"bitbucket.org/udevs/example_api_gateway/pkg/util"
	"github.com/gin-gonic/gin"
)

// Create Position godoc
// @ID create_position
// @Router /v1/position [POST]
// @Summary Create Position
// @Description Create Position
// @Tags position
// @Accept json
// @Produce json
// @Param position body models.CreatePositionModel true "position"
// @Success 200 {object} models.ResponseModel{data=string} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) CreatePosition(c *gin.Context) {
	var position models.CreatePositionModel

	err := c.BindJSON(&position)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while json", err)
		return
	}

	poss_att_array := []*position_service.PositionAttribute{}

	for _, v := range position.PositionAttribute {
		pos_att := &position_service.PositionAttribute{
			AttributeId: v.AttributeId,
			Value:       v.Value,
		}

		poss_att_array = append(poss_att_array, pos_att)
	}

	resp, err := h.services.PositionService().Create(
		context.Background(),

		&position_service.CreatePosition{
			Name:               position.Name,
			CompanyId:          position.ProfessionId,
			ProfessionId:       position.CompanyId,
			PositionAttributes: poss_att_array,
		},
	)

	if !handleError(h.log, c, err, "error while creating position") {
		fmt.Println(err.Error())
		fmt.Println("???????????????????????", position.Name)
		fmt.Println("???????????????????????", position.ProfessionId)
		return
	}
	fmt.Println("???????????????????????", position.CompanyId)
	fmt.Println("???????????????????????", position.PositionAttribute)

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// Get Position godoc
// @ID get-position
// @Router /v1/position/{position_id} [GET]
// @Summary get position
// @Description Get Position
// @Tags position
// @Accept json
// @Produce json
// @Param position_id path string true "position_id"
// @Success 200 {object} models.ResponseModel{data=models.Position} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetPosition(c *gin.Context) {
	var position models.Position
	position_id := c.Param("position_id")

	if !util.IsValidUUID(position_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "position id is not valid", errors.New("position id is not valid"))
		return
	}

	resp, err := h.services.PositionService().Get(
		context.Background(),
		&position_service.PositionId{
			Id: position_id,
		},
	)

	if !handleError(h.log, c, err, "error while getting position") {
		return
	}

	err = ParseToStruct(&position, resp)
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while parsing to struct", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// Get All Position godoc
// @ID get-all-position
// @Router /v1/position [GET]
// @Summary get all position
// @Description Get All Position
// @Tags position
// @Accept json
// @Produce json
// @Param name query string false "name"
// @Param limit query string false "limit"
// @Param offset query string false "offset"
// @Param profession_id query string false "profession_id"
// @Param company_id query string false "company_id"
// @Success 200 {object} models.ResponseModel{data=models.GetAllPositionResponse} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAllPositions(c *gin.Context) {
	var positions models.GetAllPositionResponse

	limit, err := h.ParseQueryParam(c, "limit", "10")
	if err != nil {
		return
	}

	offset, err := h.ParseQueryParam(c, "offset", "0")
	if err != nil {
		return
	}

	resp, err := h.services.PositionService().GetAll(
		context.Background(),
		&position_service.GetAllPositionRequest{
			Limit:        uint32(limit),
			Offset:       uint32(offset),
			Name:         c.Query("name"),
			ProfessionId: c.Query("profession_id"),
			CompanyId:    c.Query("company_id"),
		},
	)

	if !handleError(h.log, c, err, "error while getting all positions") {
		return
	}

	err = ParseToStruct(&positions, resp)
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while parsing to struct", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// Update Position godoc
// @ID update-position
// @Router /v1/position/{position_id} [PUT]
// @Summary update position
// @Description Update Position
// @Tags position
// @Accept json
// @Produce json
// @Param position_id path string true "position_id"
// @Param position body models.CreatePositionModel true "position"
// @Success 200 {object} models.ResponseModel{data=models.Position} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) UpdatePosition(c *gin.Context) {
	var position models.UpdatePosition
	position_id := c.Param("position_id")
	if !util.IsValidUUID(position_id) {
		h.handleErrorResponse(c, 400, "validation err", errors.New("position_id is not valid"))
		return
	}

	err := c.ShouldBind(&position)
	if err != nil {
		h.BadRequestResponse(c, err)
	}
	var pos_attrss []*position_service.PositionAttribute

	for _, v := range position.PositionAttribute {

		pos_attr := position_service.PositionAttribute{
			AttributeId: v.AttributeId,
			Value:       v.Value,
		}
		pos_attrss = append(pos_attrss, &pos_attr)

	}

	resp, err := h.services.PositionService().Update(
		context.Background(),
		&position_service.UpdatePosition{
			Id:                 position_id,
			Name:               position.Name,
			ProfessionId:       position.ProfessionId,
			CompanyId:          position.CompanyId,
			PositionAttributes: pos_attrss,
		})

	if !handleError(h.log, c, err, "error while updating profession") {
		return
	}

	err = ParseToStruct(&position, resp)
	if err != nil {

		h.handleErrorResponse(c, http.StatusInternalServerError, "error while parsing to struct", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp.Id)

}

// Delete Position godoc
// @ID delete-position
// @Router /v1/position/{position_id} [DELETE]
// @Summary delete position
// @Description Delete Position
// @Tags position
// @Accept json
// @Produce json
// @Param position_id path string true "position_id"
// @Success 200 {object} models.ResponseModel{data=string} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) DeletePosition(c *gin.Context) {
	position_id := c.Param("position_id")
	if !util.IsValidUUID(position_id) {
		h.handleErrorResponse(c, 400, "valid error", errors.New("position_id is not valid"))
		return
	}

	_, err := h.services.PositionService().Delete(
		context.Background(),
		&position_service.PositionId{Id: position_id})

	if !handleError(h.log, c, err, "error wile deleting profession") {
		return
	}

	h.handleSuccessResponse(c, 200, "ok", "Successfully deleted")
}

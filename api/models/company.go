 
 package models
 
 type CreateCompanyModel struct {
 	Name string `json:"name" binding:"required"`
 }
 
 type Company struct {
 	Id   string `json:"id"`
 	Name string `json:"name"`
 }
 
 type GetAllCompanyResponse struct {
 	Professions []Profession `json:"professions"`
 	Count       uint32       `json:"count"`
 }
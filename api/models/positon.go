package models

type CreatePositionModel struct {
	Name              string              `json:"name" binding:"required"`
	ProfessionId      string              `json:"profession_id" binding:"required"`
	CompanyId         string              `json:"company_id" binding:"required"`
	PositionAttribute []PositionAttribute `json:"position_attributes"`
}

type Position struct {
	Id                   string                 `json:"id"`
	Name                 string                 `json:"name"`
	ProfessionId         string                 `json:"profession_id"`
	CompanyId            string                 `json:"company_id"`
	GetPositionAttribute []GetPositionAttribute `json:"position_attributes"`
}

type GetAllPositionResponse struct {
	Position []Position `json:"positions"`
	Count    uint32     `json:"count"`
}

type PositionAttribute struct {
	AttributeId string `json:"attribute_id" binding:"required"`
	Value       string `json:"value" binding:"required"`
}

type GetPositionAttribute struct {
	Id         string    `json:"id" binding:"required"`
	AtributeId string    `json:"attribute_id" binding:"required"`
	PositionId string    `json:"position_id" binding:"required"`
	Value      string    `json:"value" binding:"required"`
	Attribute  Attribute `json:"attribute" binding:"required"`
}

type UpdatePosition struct {
	Id           string `json:"id"`
	Name         string `json:"name"`
	ProfessionId string `json:"profession_id"`
	CompanyId    string `json:"company_id"`
	PositionAttribute []PositionAttribute `json:"position_attributes"`
}

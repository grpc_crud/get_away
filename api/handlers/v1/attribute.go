package v1

import (
	"context"
	"errors"
	"net/http"

	"bitbucket.org/udevs/example_api_gateway/api/models"
	"bitbucket.org/udevs/example_api_gateway/genproto/position_service"
	"bitbucket.org/udevs/example_api_gateway/pkg/util"
	"github.com/gin-gonic/gin"
)

// Create Attribute godoc
// @ID attribute_proession
// @Router /v1/attribute [POST]
// @Summary Create Attribute
// @Description Create Profession
// @Tags attribute
// @Accept json
// @Produce json
// @Param attribute body models.CreateAttributeModel true "attribute"
// @Success 200 {object} models.ResponseModel{data=string} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) CreateAttribute(c *gin.Context) {
	var attribute models.CreateAttributeModel

	err := c.BindJSON(&attribute)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while json", err)
		return
	}

	resp, err := h.services.AttributeService().Create(
		context.Background(),
		&position_service.CreateAttribute{
			Name:          attribute.Name,
			AttrubiteType: attribute.AttributeType,
		},
	)

	if !handleError(h.log, c, err, "error while creating attribute") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// Get Attribute godoc
// @ID get-attribute
// @Router /v1/attribute/{attribute_id} [GET]
// @Summary get attribute
// @Description Get Attribute
// @Tags attribute
// @Accept json
// @Produce json
// @Param attribute_id path string true "attribute_id"
// @Success 200 {object} models.ResponseModel{data=models.Attribute} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAttribute(c *gin.Context) {
	var attribute models.Attribute
	attribute_id := c.Param("attribute_id")

	if !util.IsValidUUID(attribute_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "profession id is not valid", errors.New("profession id is not valid"))
		return
	}

	resp, err := h.services.AttributeService().Get(
		context.Background(),
		&position_service.AttributeId{
			Id: attribute_id,
		},
	)

	if !handleError(h.log, c, err, "error while getting profession") {
		return
	}

	err = ParseToStruct(&attribute, resp)
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while parsing to struct", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// Get All Attribute godoc
// @ID get-all-attribute
// @Router /v1/attribute [GET]
// @Summary get all attribute
// @Description Get All Attribute
// @Tags attribute
// @Accept json
// @Produce json
// @Param name query string false "name"
// @Param limit query string false "limit"
// @Param offset query string false "offset"
// @Success 200 {object} models.ResponseModel{data=models.GetAllAttributeResponse} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAllAttributes(c *gin.Context) {
	var attributes models.GetAllAttributeResponse

	limit, err := h.ParseQueryParam(c, "limit", "10")
	if err != nil {
		return
	}

	offset, err := h.ParseQueryParam(c, "offset", "0")
	if err != nil {
		return
	}

	resp, err := h.services.AttributeService().GetAll(
		context.Background(),
		&position_service.GetAllAttributeRequest{
			Limit:  uint32(limit),
			Offset: uint32(offset),
			Name:   c.Query("name"),
		},
	)

	if !handleError(h.log, c, err, "error while getting all attributes") {
		return
	}

	err = ParseToStruct(&attributes, resp)
	if err != nil {
		h.handleErrorResponse(c, http.StatusInternalServerError, "error while parsing to struct", err)
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// Update Attribute godoc
// @ID update-attribute
// @Router /v1/attribute/{attribute_id} [PUT]
// @Summary update attribute
// @Description Update Attribute
// @Tags attribute
// @Accept json
// @Produce json
// @Param attribute_id path string true "attribute_id"
// @Param profession body models.CreateAttributeModel true "attribute"
// @Success 200 {object} models.ResponseModel{data=models.Attribute} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) UpdateAttribute(c *gin.Context) {
	var attribute models.Attribute
	attribute_id := c.Param("attribute_id")
	if !util.IsValidUUID(attribute_id) {
		h.handleErrorResponse(c, 400, "validation err", errors.New("attribute_id is not valid"))
		return
	}

	err := c.ShouldBind(&attribute)
	if err != nil {
		h.BadRequestResponse(c, err)
	}

	resp, err := h.services.AttributeService().Update(
		context.Background(),
		&position_service.Attribute{
			Id:            attribute_id,
			Name:          attribute.Name,
			AttributeType: attribute.AttributeType,
		})

	if !handleError(h.log, c, err, "error while updating Attribute") {
		return
	}

	err = ParseToStruct(&attribute, resp)
	if err != nil {

		h.handleErrorResponse(c, http.StatusInternalServerError, "error while parsing to struct", err)
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)

}

// Delete Attribute godoc
// @ID delete-attribute
// @Router /v1/attribute/{attribute_id} [DELETE]
// @Summary delete attribute
// @Description Delete Attribute
// @Tags attribute
// @Accept json
// @Produce json
// @Param attribute_id path string true "attribute_id"
// @Success 200 {object} models.ResponseModel{data=string} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) DeleteAttribute(c *gin.Context) {
	attribute_id := c.Param("attribute_id")
	if !util.IsValidUUID(attribute_id) {
		h.handleErrorResponse(c, 400, "valid error", errors.New("attribute_id is not valid"))
		return
	}

	_, err := h.services.AttributeService().Delete(
		context.Background(),
		&position_service.AttributeId{Id: attribute_id})

	if !handleError(h.log, c, err, "error wile deleting attribute") {
		return
	}

	h.handleSuccessResponse(c, 200, "ok", "Successfully deleted")
}
